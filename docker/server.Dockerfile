FROM php:7.3-fpm

ENV PG_MAJOR 9.6

# Install postgresql-client for connecting to DB
# RUN apt-get update -y && apt-get install -y --no-install-recommends gnupg2 wget ca-certificates
# RUN wget -qO- https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
# RUN echo 'deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main' $PG_MAJOR > /etc/apt/sources.list.d/pgdg.list

# Install required packages
RUN apt-get update && apt-get install -y \
      zip \
      git \
      libpq-dev \
    && rm -rf /var/lib/apt/lists/*

# Configure pgsql with PHP
# RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql
RUN docker-php-ext-install pdo pdo_pgsql opcache

# Install PHP XDebug
# RUN pecl install xdebug
# RUN echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20170718/xdebug.so" >> /usr/local/etc/php/conf.d/xdebug-php.ini
# RUN docker-php-ext-install xdebug

# Install OPcache Config
COPY ./conf-dev/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

EXPOSE 8080
