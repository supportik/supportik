Write-Output "Create Dev Machine"
docker-machine create --virtualbox-share-folder "C:/devel:/c/devel" --virtualbox-cpu-count -1 --virtualbox-memory 2048 default

Write-Output "Start/Restart Dev Container"

Write-Output "  Start Docker Machine"
docker-machine start >$null 2>$null

Write-Output "  Set Docker Machine ENV vars"
& docker-machine env | Invoke-Expression
$IP = docker-machine ip

Write-Output "  Docker Compose"
docker-compose up -d --build

Write-Output "DONE!"

Write-Output ""
Write-Output "Go to http://supportik.devel (if configured)"
Write-Output "  else go to http://$IP"