@echo off

echo Login to GitLab Container Registry
docker login registry.gitlab.com

IF %ERRORLEVEL% NEQ 0 (
  exit /B %ERRORLEVEL%
)

echo Build image
docker build -q -t registry.gitlab.com/supportik/supportik:dev . >nul

IF %ERRORLEVEL% NEQ 0 (
  exit /B %ERRORLEVEL%
)

echo Push image to registry
docker push registry.gitlab.com/supportik/supportik:dev

echo Logoff
docker logout registry.gitlab.com >nul
