# Supportik

Supportik is an open source support, ticketing, and helpdesk application.

## Development Environment Setup

**Assumptions**
- Your development repositories are kept in `C:/devel` which already exists
- You are using `powershell` as your shell

**Steps**

1. Setup Docker

   *Skip this step if you already have docker installed*

   1. Download/Install Docker

      [Docker Desktop on Windows (Hyper-V support required)](https://docs.docker.com/docker-for-windows/install/)
      [Docker Desktop on Mac (OS 10.10 or greater required)](https://docs.docker.com/docker-for-mac/install/)

      [Docker Toolbox on Windows](https://docs.docker.com/toolbox/toolbox_install_windows/)
      [Docker Toolbox on Mac](https://docs.docker.com/toolbox/toolbox_install_mac/)

      [Linux](https://runnable.com/docker/install-docker-on-linux)

   2. Docker Machine Setup (for Docker Toolbox)
      **This step is only required if you are using Docker Toolbox**

      *It assumed you are using VirtualBox*

      ```ps1
      PS> docker-machine create\
        -d virtualbox\
        --virtualbox-share-folder "C:/devel:/c/devel"\
        --virtualbox-cpu-count -1\
        --virtualbox-memory 2048\
        default

      PS> docker-machine ls
      NAME      ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER     ERRORS
      default   *        virtualbox   Running   tcp://192.168.99.100:2376           v19.03.5

      PS> & docker-machine env | Invoke-Expression
      ```

   3. Test Docker

      ```ps1
      PS> docker ps
      CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
      ```

2. Clone Repository

   ```ps1
   PS> cd C:/devel
   PS> git clone <URL> supportik
   ```

3. Start containers

   ```ps1
   PS> cd C:/devel/supportik
   PS> docker-compose up -d --build
   ```

4. Setup Hosts

   [See this guide for help doing this.](https://www.howtogeek.com/howto/27350/beginner-geek-how-to-edit-your-hosts-file/)

   Add the following line to your hosts file:
   ```
   127.0.0.1	supportik.local
   ```
   **For Docker Toolbox**, use the IP from `docker-machine ip`:
   ```
   192.168.99.100	supportik.local
   ```

5. Open application

   Navtigate to `http://supportik.local` in your faviourite browser.

## Notes

@TODO