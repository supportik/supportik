<?php

namespace App;

use Model\User;
use App\User\UserMediator;
use Arrow\MiddlewareInterface;
use Arrow\Event\RoutePreCallEvent;
use League\Event\Emitter;
use League\Route\Route;
use League\Route\Http\Exception\UnauthorizedException;
use Psr\Http\Message\ServerRequestInterface;

class ControllerAuthMiddleware implements MiddlewareInterface {


	public function process(\Arrow\Application $app): void {
		// $app->get(Emitter::class)->addListener(
		//     RoutePreCallEvent::class,
		//     function (RoutePreCallEvent $event) use ($app) {
		//         $this->doAuthCheck($app, $event);
		//     }
		// );
	}

	private function doAuthCheck($app, RoutePreCallEvent $event) {
		if (!$this->isAuthCheckRequired($event->Route)) {
			return;
		}

		$userMediator = $app->container()->get('user.mediator');
		$user = $this->getUserLoggedIn($event->Request, $userMediator);

		if (!$user) {
			throw new UnauthorizedException('Auth Required');
		}

		$event->Request = $event->Request->withAttribute('User', $user);
	}

	private function isAuthCheckRequired(Route $route): bool {
		$callable = $route->getCallable();

		if (!is_array($callable) || !is_object($callable[0])) {
			return false;
		}

		if (!($callable[0] instanceof ControllerAuthRequiredInterface)) {
			return false;
		}

		return true;
	}

	private function getUserLoggedIn(ServerRequestInterface $request, UserMediator $userMediator): ?User {
		$sessionID = $request->getCookieParams()['APPSESSIONID'] ?? null;

		return $userMediator->getUserLoggedIn($sessionID);
	}
}
