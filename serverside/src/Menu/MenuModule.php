<?php

declare(strict_types=1);

namespace App\Menu;

use App\Constant as App;
use Arrow\Config;
use Arrow\ModuleWebInterface;
use Arrow\Router;
use Arrow\Router\RouteCollector;
use League\Container\Container;

class MenuModule implements ModuleWebInterface {


	public function registerRoutes(Router $router): void {
		$router->group('/api/menu', function (RouteCollector $group) {
			$group->get('/', 'menu.controller::list');
		});
	}

	public function registerServices(Container $container, Config $config): void {
		$container->share('menu.controller', MenuController::class)
			->addArgument('user.mediator');
	}
}
