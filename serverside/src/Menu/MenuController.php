<?php

declare(strict_types=1);

namespace App\Menu;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\User\UserMediator;

class MenuController {


	/**
	 * @var \App\User\UserMediator
	 */
	private $userMediator;

	public function __construct(UserMediator $userMediator) {
		$this->userMediator = $userMediator;
	}

	public function list(ServerRequestInterface $request, ResponseInterface $response) {
		$sessionID = $request->getCookieParams()['APPSESSIONID'] ?? null;
		if (!$this->userMediator->getUserLoggedIn($sessionID)) {
			return $this->returnItems($response, [
				['Path' => '/login', 'Text' => 'Login', 'Icon' => 'unlock'],
			]);
		}

		$items = [
			['Path' => '/app/dashboard', 'Text' => 'Dashboard', 'Icon' => 'home'],
			['Path' => '/admin', 'Text' => 'Admin', 'Icon' => 'stats-up'],
			['Path' => '/logout', 'Text' => 'Logout', 'Icon' => 'unlock'],
		];

		return $this->returnItems($response, $items);
	}

	private function returnItems($response, $items) {
		$response = $response->withStatus(200);
		$response->getBody()->write(json_encode([
			'Payload' => [
				'Items' => $items,
			],
		]));

		return $response;
	}
}
