<?php

declare(strict_types=1);

namespace App\NotFound;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class NotFoundController {


	public function notFoundController(ServerRequestInterface $request, ResponseInterface $response) {
		$response = $response->withStatus(200);
		$response->getBody()->write(json_encode(['Error' => 'Invalid Route (controller).']));

		return $response;
	}

	public function notFoundMethod(ServerRequestInterface $request, ResponseInterface $response) {
		$response = $response->withStatus(200);
		$response->getBody()->write(json_encode(['Error' => 'Invalid Route (method).']));

		return $response;
	}
}
