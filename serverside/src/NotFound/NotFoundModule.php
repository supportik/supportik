<?php

declare(strict_types=1);

namespace App\NotFound;

use App\Constant;
use Arrow\Config;
use Arrow\ModuleWebInterface;
use Arrow\Router;
use League\Container\Container;

class NotFoundModule implements ModuleWebInterface {


	public function registerRoutes(Router $router): void {
	}

	public function registerServices(Container $container, Config $config): void {
		// $container->share(Constant::CONTAINER_MODULE_NOTFOUND_CONTROLLER, NotFoundController::class);
	}
}
