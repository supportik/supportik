<?php

declare(strict_types=1);

namespace App\CLI;

use App\CLI\CLIDocker;
use Arrow\Config;
use Arrow\CLI\CLI;
use Arrow\CLI\ModuleInterface as ModuleCLIInterface;
use League\Container\Container;

class CLIModule implements ModuleCLIInterface {


	public function registerCli(CLI $cli, Container $container, Config $config): void {
	}

	public function registerServices(Container $container, Config $config): void {
	}
}
