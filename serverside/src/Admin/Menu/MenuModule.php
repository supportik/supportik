<?php

declare(strict_types=1);

namespace App\Admin\Menu;

use Arrow\Config;
use Arrow\ModuleWebInterface;
use Arrow\Router;
use Arrow\Router\RouteCollector;
use League\Container\Container;

class MenuModule implements ModuleWebInterface {


	public function registerRoutes(Router $router): void {
		$router->group('/api/admin/menu', function (RouteCollector $group) {
			$group->get('/', 'admin.menu.controller::list');
		});
	}

	public function registerServices(Container $container, Config $config): void {
		$container->share('admin.menu.controller', MenuController::class)
			->addArgument('user.mediator');
	}
}
