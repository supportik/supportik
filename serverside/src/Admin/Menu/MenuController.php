<?php

declare(strict_types=1);

namespace App\Admin\Menu;

use App\Constant;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\User\UserMediator;
use League\Route\Http\Exception\UnauthorizedException;
use Model\User;

class MenuController {


	/**
	 * @var \App\User\UserMediator
	 */
	private $userMediator;

	public function __construct(UserMediator $userMediator) {
		$this->userMediator = $userMediator;
	}

	public function list(ServerRequestInterface $request, ResponseInterface $response) {
		// $this->checkAuthAdmin($request);

		$sessionID = $request->getCookieParams()['APPSESSIONID'] ?? null;

		if (!$this->userMediator->getUserLoggedIn($sessionID)) {
			return $this->returnItems($response, [
				['Path' => '/login', 'Text' => 'Login', 'Icon' => 'unlock'],
			]);
		}

		$items = [
			['Path' => '/admin/dashboard', 'Text' => 'Dashboard', 'Icon' => 'stats-up'],
			['Path' => '/app/dashboard', 'Text' => 'Dashboard', 'Icon' => 'home'],
			['Path' => '/logout', 'Text' => 'Logout', 'Icon' => 'unlock'],
		];

		return $this->returnItems($response, $items);
	}

	private function returnItems($response, $items) {
		$response = $response->withStatus(200);
		$response->getBody()->write(json_encode([
			'Payload' => [
				'Items' => $items,
			],
		]));

		return $response;
	}

	private function checkAuthAdmin(ServerRequestInterface $request) {
		$user = $request->getAttribute('User');

		if (!$user) {
			throw new UnauthorizedException();
		} elseif ($user->getType() != Constant::USER_ROLE_ADMIN) {
			throw new UnauthorizedException();
		}
	}
}
