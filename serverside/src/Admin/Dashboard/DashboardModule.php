<?php

declare(strict_types=1);

namespace App\Admin\Dashboard;

use App\Constant as App;
use Arrow\Config;
use Arrow\ModuleWebInterface;
use Arrow\Router;
use Arrow\Router\RouteCollector;
use League\Container\Container;

class DashboardModule implements ModuleWebInterface {


	public function registerRoutes(Router $router): void {
		$router->group('/api/admin/dashboard', function (RouteCollector $group) {
			$group->get('/', 'admin.dashboard.controller::fetchWidgets');
		});
	}

	public function registerServices(Container $container, Config $config): void {
		$container->share('admin.dashboard.controller', DashboardController::class);
	}
}
