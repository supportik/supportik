<?php

declare(strict_types=1);

namespace App\Admin\Dashboard;

use App\ControllerAuthRequiredInterface;
use Arrow\Exception;
use Model\User;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class DashboardController implements ControllerAuthRequiredInterface {


	public function __construct() {
	}

	public function fetchWidgets(ServerRequestInterface $request, ResponseInterface $response) {
		$response = $response->withStatus(200);
		$response->getBody()->write(json_encode([
			'Payload' => [
				'Widgets' => [],
			],
		]));

		return $response;
	}
}
