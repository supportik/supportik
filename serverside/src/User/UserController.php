<?php

declare(strict_types=1);

namespace App\User;

use App\ControllerAuthRequiredInterface;
use Arrow\Exception;
use Model\User;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class UserController implements ControllerAuthRequiredInterface {


	/**
	 * @var UserDomain
	 */
	private $userDomain;

	public function __construct(UserDomain $userDomain) {
		$this->userDomain = $userDomain;
	}

	public function listUsers(ServerRequestInterface $request, ResponseInterface $response) {
		$response = $response->withStatus(200);
		$response->getBody()->write(json_encode([
			'Payload' => [
				'Users' => $this->userDomain->getUsers()->toArray(),
			],
		]));

		return $response;
	}

	public function getUser(ServerRequestInterface $request, ResponseInterface $response, $vars) {
		$user = $this->userDomain->getUserByUserID($vars['id']);

		if (!$user) {
			throw new Exception("Unknown User");
		}

		$response = $response->withStatus(200);
		$response->getBody()->write(json_encode([
			'Payload' => [
				'User' => $user->toArray(),
			],
		]));

		return $response;
	}

	public function saveUser(ServerRequestInterface $request, ResponseInterface $response) {
		$userData = json_decode($request->getBody()->getContents(), true);

		$user = $this->userDomain->saveUser($userData);

		$response = $response->withStatus(200);
		$response->getBody()->write(json_encode([
		  'Payload' => [
			  'User' => $user->toArray(),
		  ],
		]));

		return $response;
	}
}
