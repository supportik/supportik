<?php

declare(strict_types=1);

namespace App\User;

use App\Constant;
use App\ControllerAuthRequiredInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class UserLoginController {


  /**
   * @var \App\User\UserDomain
   */
	private $userDomain;
  /**
   * @var \App\User\UserLoginDomain
   */
	private $userLoginDomain;

	public function __construct(UserDomain $userDomain, UserLoginDomain $userLoginDomain) {
		$this->userDomain = $userDomain;
		$this->userLoginDomain = $userLoginDomain;
	}

	public function validate(ServerRequestInterface $request, ResponseInterface $response) {
		$sessionID = ($request->getCookieParams()['APPSESSIONID'] ?? null);
		$user = $this->userLoginDomain->getUserLoggedIn($sessionID);
		$userSession = $this->userLoginDomain->getUserSessionBySessionID($sessionID);

		if (!$user) {
			$response->getBody()->write(json_encode([
				'Payload' => [
					'Valid' => false,
					'User' => null,
					'Expires' => null,
				],
			]));
		} else {
			$response->getBody()->write(json_encode([
			'Payload' => [
				'Valid' => true,
				'User' => $user->toArray(),
				'Expires' => $userSession->getExpires(),
			],
			]));
		}

		$response = $response->withStatus(200);
		return $response;
	}

	public function login(ServerRequestInterface $request, ResponseInterface $response) {
		$body = json_decode($request->getBody()->getContents(), true);
		$user = $this->userLoginDomain->checkLoginDetails($body['email'], $body['password']);

		if ($user) {
			if ($sessionID = ($request->getCookieParams()['APPSESSIONID'] ?? null)) {
				$this->userLoginDomain->logoutUserSession($sessionID);
			}

			$this->userLoginDomain->loginUser($user);
		}

		$response = $response->withStatus(200);
		$response->getBody()->write(json_encode([
		'Payload' => [
		'Success' => !!$user,
		],
		]));

		return $response;
	}

	public function loginAdmin(ServerRequestInterface $request, ResponseInterface $response) {
		$body = json_decode($request->getBody()->getContents(), true);
		$user = $this->userLoginDomain->checkLoginDetails($body['email'], $body['password']);

		if ($user && $user->getType() == Constant::USER_ROLE_ADMIN) {
			if ($sessionID = ($request->getCookieParams()['APPSESSIONID'] ?? null)) {
				$this->userLoginDomain->logoutUserSession($sessionID);
			}

			$this->userLoginDomain->loginUser($user);
		} else {
			$user = null;
		}

		$response = $response->withStatus(200);
		$response->getBody()->write(json_encode([
		'Payload' => [
		'Success' => !!$user,
		],
		]));

		return $response;
	}

	public function logout(ServerRequestInterface $request, ResponseInterface $response) {
		$sessionID = ($request->getCookieParams()['APPSESSIONID'] ?? null);
		$this->userLoginDomain->logoutUserSession($sessionID);

		$response = $response->withStatus(200);
		$response->getBody()->write(json_encode([
		'Payload' => [
		'Success' => true,
		],
		]));

		return $response;
	}
}
