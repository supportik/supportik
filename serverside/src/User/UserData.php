<?php

declare(strict_types=1);

namespace App\User;

use Model\UserQuery;
use Model\UserSessionQuery;
use Model\User;
use Model\UserSession;
use Arrow\Propel\ExecSQL;
use Propel\Runtime\Collection\Collection;

class UserData {
	use ExecSQL;

	public function getUsers(): Collection {
		return UserQuery::create()
			->find();
	}

	public function getUserByUserID($userID): ?User {
		return UserQuery::create()
			->findOneByUserID($userID);
	}

	public function getUserByEmail($email): ?User {
		return UserQuery::create()
			->findOneByEmail($email);
	}

	public function getUserSessionBySessionID($sessionID): ?UserSession {
		return UserSessionQuery::create()
			->findOneBySessionID($sessionID);
	}

	public function getUserSessionByUserID($userID): ?UserSession {
		return UserSessionQuery::create()
			->findOneByUserID($sessionID);
	}

	public function updateUserSession(int $userID, string $sessionID): void {
		$bind = [
			'UserID' => $userID,
			'SessionID' => $sessionID,
		];

		$sql = <<<SQL
WITH DeleteDuplicateSession AS (
    DELETE FROM usersession
    WHERE sessionid = :SessionID
    RETURNING 1
)
, CreateUserSession AS (
    INSERT INTO usersession (userid, sessionid, expires)
    SELECT :UserID, :SessionID, CURRENT_TIMESTAMP + '1 hour'::INTERVAL
    WHERE NOT EXISTS (SELECT 1 FROM usersession WHERE userid = :UserID)
    RETURNING 1
)
, UpdateUserSession AS (
    UPDATE usersession SET sessionID = :SessionID, expires = CURRENT_TIMESTAMP + '1 hour'::INTERVAL
    WHERE userID = :UserID
    RETURNING 1
)
SELECT * FROM DeleteDuplicateSession
UNION ALL
SELECT * FROM CreateUserSession
UNION ALL
SELECT * FROM UpdateUserSession;
SQL;

		$this->execSQL($sql, $bind);
	}

	public function clearUserSession($sessionID): void {
		UserSessionQuery::create()
			->filterBySessionID($sessionID)
			->delete();
	}
}
