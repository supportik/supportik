<?php

declare(strict_types=1);

namespace App\User;

use Model\Map\UserTableMap;
use Model\User;

class UserDomain {


	private $userData;

	public function __construct(UserData $userData) {
		$this->userData = $userData;
	}

	public function getUsers() {
		return $this->userData->getUsers();
	}

	public function getUserByUserID($userID) {
		return $this->userData->getUserByUserID($userID);
	}

	public function checkUserLogin($username, $password) {
		$user = $this->userData->getUserByEmail($username);

		if (!$user) {
			return false;
		}

		// TODO password checking

		$this->userData->updateUserSession($user['UserID'], $user['UserID']);

		return true;
	}

	public function saveUser($data): User {
		if (empty($data['UserID'])) {
			$user = new User();
		} else {
			$user = $this->userData->getUserByUserID($data['UserID']);
		}

		unset($data['Password']);

		$user->fromArray($data);
		$user->save();

		return $user;
	}

	public function logoutUser($sessionID) {
		$this->userData->clearUserSession($sessionID);
	}
}
