<?php

namespace App\User;

use League\Container\Container;
use Model\User;

class UserMediator {


	private $container;

	public function __construct(Container $container) {
		$this->container = $container;
	}

	public function getUserLoggedIn(?string $sessionID): ?User {
		return $this->container->get('user.domain.login')
			->getUserLoggedIn($sessionID);
	}
}
