<?php

declare(strict_types=1);

namespace App\User;

use App\Constant as App;
use Arrow\Config;
use Arrow\ModuleWebInterface;
use Arrow\Router;
use Arrow\Router\RouteCollector;
use League\Container\Container;

class UserModule implements ModuleWebInterface {


	public function registerRoutes(Router $router): void {
		$router->group('/api/user', function (RouteCollector $group) {
			$group->post('/', 'user.controller::saveUser');
			$group->get('/{id:number}', 'user.controller::getUser');
			$group->get('/list', 'user.controller::listUsers');
			$group->post('/login', 'user.controller.login::login');
			$group->get('/logout', 'user.controller.login::logout');
			$group->get('/validate', 'user.controller.login::validate');
		});

		$router->group('/api/user/admin', function (RouteCollector $group) {
			$group->post('/login', 'user.controller.login::loginAdmin');
		});
	}

	public function registerServices(Container $container, Config $config): void {
		$container->share('user.mediator', UserMediator::class)
			->addArgument($container);

		$container->share('user.controller', UserController::class)
			->addArgument('user.domain');

		$container->share('user.controller.login', UserLoginController::class)
			->addArguments(['user.domain', 'user.domain.login']);

		$container->share('user.domain', UserDomain::class)
			->addArgument('user.data');

		$container->share('user.domain.login', UserLoginDomain::class)
			->addArgument('user.data');

		$container->share('user.data', UserData::class);
	}
}
