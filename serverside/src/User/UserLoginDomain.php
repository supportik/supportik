<?php

namespace App\User;

use Model\User;
use Model\UserSession;

class UserLoginDomain {


	/**
	 * @var \App\User\UserData
	 */
	private $userData;

	public function __construct($userData) {
		$this->userData = $userData;
	}

	public function loginUser(User $user): void {
		$sessionID = base64_encode(random_bytes(12));
		if (!setcookie('APPSESSIONID', $sessionID, time() + (60 * 60 * 24 * 30), '/')) {
			throw new \Exception('Could not set cookie.');
		}
		$this->userData->updateUserSession($user->getUserID(), $sessionID);
	}

	public function logoutUserSession($sessionID): void {
		if ($sessionID) {
			$this->userData->clearUserSession($sessionID);
		}
	}

	public function checkLoginDetails($email, $password): ?User {
		$user = $this->userData->getUserByEmail($email);

		if (!$user) {
			return null;
		} elseif (!$user->getActive()) {
			return null;
		} elseif (!password_verify($password, $user->getPassword())) {
			return null;
		}

		return $user;
	}

	public function getUserLoggedIn(?string $sessionID): ?User {
		if (!$sessionID) {
			return null;
		}

		$userSession = $this->getUserSessionBySessionID($sessionID);

		if (!$userSession) {
			return null;
		} elseif ($userSession->getExpires()->getTimestamp() < time()) {
			return null;
		}

		return $this->userData->getUserByUserID($userSession->getUserID());
	}

	public function getUserSessionBySessionID(?string $sessionID): ?UserSession {
		return $this->userData->getUserSessionBySessionID($sessionID);
	}
}
