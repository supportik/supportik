<?php

namespace App;

use Arrow\Config;
use Arrow\MiddlewareInterface;
use Arrow\Event\ApplicationHandleExceptionEvent;
use Arrow\Event\ResponseFormatJsonEvent;
use League\Event\Emitter;
use League\Route\Http\Exception\UnauthorizedException;
use Psr\Http\Message\StreamInterface;

use function GuzzleHttp\Psr7\stream_for;

class ErrorHandlerMiddleware implements MiddlewareInterface {

	public function process(\Arrow\Application $app): void {
		$app->get(Emitter::class)->addListener(
			ApplicationHandleExceptionEvent::class,
			function (ApplicationHandleExceptionEvent $event) use ($app) {
				$this->handleException($app, $event, $app->get(Config::class));
			}
		);
	}

	private function handleException(
		\Arrow\Application $app,
		ApplicationHandleExceptionEvent $event,
		Config $config
	): void {
		if ($event->Exception instanceof UnauthorizedException) {
			// $event->LogException = false;
			$event->Response = $event->Response
				->withStatus(401) // Unauthorized
				->withBody($this->buildJsonResponse($app, $event->Request, [
					'Error' => [
						"Status" => 401,
						"Message" => "Unauthorized",
					]
				], $event->Exception));

			return;
		}

		if ($config->get('Debug')) {
			$event->Response
				->withBody($this->buildJsonResponse($app, $event->Request, [
					'InternalError' => [
						"Status" => 500,
						"Message" => $event->Exception->getMessage(),
						"File" => "{$event->Exception->getFile()}({$event->Exception->getLine()})",
						"Trace" => $event->Exception->getTraceAsString(),
					],
				], $event->Exception));
		}
	}

	private function buildJsonResponse($app, $request, $data, $exception = null): StreamInterface {
		$app->events()->emit($event = new ResponseFormatJsonEvent($request, $data, $exception));

		return stream_for(json_encode($event->Data));
	}
}
