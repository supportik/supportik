<?php

declare(strict_types=1);

namespace App;

class Constant extends \Arrow\Constant {


	const USER_ROLE_USER = 'User';
	const USER_ROLE_ADMIN = 'Admin';
}
