<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1572073568.
 * Generated on 2019-10-26 07:06:08 by root
 */
class PropelMigration_1572073568_init
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        $pdo = $manager->getAdapterConnection('default');
        $pdo->exec(<<<SQL
BEGIN;

CREATE SEQUENCE id_sequence START 10000 INCREMENT 1;

ALTER TABLE id ALTER COLUMN id TYPE int;
ALTER TABLE id ALTER COLUMN id SET DEFAULT nextval('id_sequence');

INSERT INTO client (clientid, fullname, alias)
VALUES (1, 'Supportik', 'Supportik');

INSERT INTO user_ (userid, clientid, fullname, displayname, email, password)
VALUES (2, 1, 'System User', 'System', 'system', '\$argon2i\$v=19\$m=1024,t=2,p=2\$NVdMREU5N1dwYTZqZURMWQ\$zIrRXX7ITxdMYsCnrg8GRWrF1AasFL1u8m9v9oPiBCo');

INSERT INTO id (id)
VALUES (1), (2);

COMMIT;
SQL
        );
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        $pdo = $manager->getAdapterConnection('default');
        $pdo->exec(<<<SQL
BEGIN;

DROP SEQUENCE IF EXISTS id_sequence;

COMMIT;
SQL
        );
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => <<<SQL
BEGIN;

CREATE TABLE "client"
(
    "clientid" INTEGER NOT NULL,
    "fullname" TEXT NOT NULL,
    "alias" TEXT,
    PRIMARY KEY ("clientid")
);

CREATE TABLE "id"
(
    "id" serial NOT NULL,
    "created" TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    "userid" INTEGER DEFAULT 2 NOT NULL,
    PRIMARY KEY ("id")
);

CREATE TABLE "user_"
(
    "userid" INTEGER NOT NULL,
    "clientid" INTEGER NOT NULL,
    "fullname" TEXT NOT NULL,
    "displayname" TEXT,
    "email" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "active" BOOLEAN DEFAULT TRUE,
    PRIMARY KEY ("userid"),
    CONSTRAINT "user_un_email" UNIQUE ("email")
);

CREATE INDEX "user_idx_clientid" ON "user_" ("clientid");

CREATE TABLE "usersession"
(
    "userid" INTEGER NOT NULL,
    "sessionid" TEXT NOT NULL,
    "expires" TIMESTAMP NOT NULL,
    "data" VARCHAR,
    PRIMARY KEY ("userid"),
    CONSTRAINT "usersession_idx_sessionid" UNIQUE ("sessionid")
);

ALTER TABLE "id" ADD CONSTRAINT "id_fk_userid"
    FOREIGN KEY ("userid")
    REFERENCES "user_" ("userid");

ALTER TABLE "user_" ADD CONSTRAINT "user_fk_clientid"
    FOREIGN KEY ("clientid")
    REFERENCES "client" ("clientid");

ALTER TABLE "usersession" ADD CONSTRAINT "usersession_fk_user"
    FOREIGN KEY ("userid")
    REFERENCES "user_" ("userid");

COMMIT;
SQL
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => <<<SQL
BEGIN;

DROP TABLE IF EXISTS "client" CASCADE;

DROP TABLE IF EXISTS "id" CASCADE;

DROP TABLE IF EXISTS "migration" CASCADE;

DROP TABLE IF EXISTS "user_" CASCADE;

DROP TABLE IF EXISTS "usersession" CASCADE;

COMMIT;
SQL
);
    }

}