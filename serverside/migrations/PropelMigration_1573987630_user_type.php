<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1573987630.
 * Generated on 2019-11-17 10:47:10 by root
 */
class PropelMigration_1573987630_user_type
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
      $pdo = $manager->getAdapterConnection('default');
      $pdo->exec(<<<SQL
BEGIN;

ALTER TABLE "user_" ALTER COLUMN "type" DROP DEFAULT;

COMMIT;
SQL
      );
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => <<<SQL
BEGIN;

ALTER TABLE "user_" ADD "type" TEXT NOT NULL DEFAULT 'Admin';

COMMIT;
SQL
,
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => <<<SQL
BEGIN;

ALTER TABLE "user_" DROP COLUMN "type";

COMMIT;
SQL
);
    }

}