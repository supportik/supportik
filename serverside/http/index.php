<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new \Arrow\Application(['Debug' => true]);

$request = \GuzzleHttp\Psr7\ServerRequest::fromGlobals();
$response = new \GuzzleHttp\Psr7\Response();

$response = $app->run($request, $response);
$app->flush($response);
$app->terminate();
