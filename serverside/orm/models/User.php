<?php

namespace Model;

use Model\Base\User as BaseUser;
use Propel\Runtime\Map\TableMap;

/**
 * Skeleton subclass for representing a row from the 'user_' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class User extends BaseUser
{

    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        $array = parent::toArray();
        
        unset($array['Password']);
        
        return $array;
    }

}
