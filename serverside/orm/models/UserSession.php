<?php

namespace Model;

use Model\Base\UserSession as BaseUserSession;

/**
 * Skeleton subclass for representing a row from the 'usersession' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UserSession extends BaseUserSession
{

}
