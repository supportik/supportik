<?php 

/* GENERATED - DO NOT EDIT */

return [
	'propel' => [
		'database' => [
			'connections' => [
				'default' => [
					'adapter' => 'pgsql',
					'classname' => 'Propel\\Runtime\\Connection\\ConnectionWrapper',
					'dsn' => 'pgsql:host=db;port=5432;dbname=docker',
					'user' => 'docker',
					'password' => 'docker',
					'attributes' => [
					],
				],
			],
		],
		'runtime' => [
			'defaultConnection' => 'default',
			'connections' => [
				0 => 'default',
			],
		],
		'generator' => [
			'defaultConnection' => 'default',
			'connections' => [
				0 => 'default',
			],
			'namespaceAutoPackage' => false,
		],
		'reverse' => [
			'connection' => 'default',
		],
	],
];