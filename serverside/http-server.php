<?php

if (preg_match('/\.(?:html|png|jpg|jpeg|gif|ico|js|vue|css|eot|svg|ttf|woff)$/', $_SERVER["REQUEST_URI"])) {
    return false; // serve the requested resource as-is.
}

include "http/index.php";
