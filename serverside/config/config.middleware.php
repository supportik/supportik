<?php

use Arrow\Middleware\ModuleLoader;
use App\ControllerAuthMiddleware;
use App\ErrorHandlerMiddleware;

return [
    'Middleware' => [
        ModuleLoader::class,
        ControllerAuthMiddleware::class,
        ErrorHandlerMiddleware::class,
    ],
];
