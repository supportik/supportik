<?php

return [
    'Debug' => true,

    'Package' => [
        'arrowphp/propel' => [
            'Propel' => [
                'database' => [
                    'connections' => [
                        'default' => [
                            'adapter' => 'pgsql',
                            // 'classname' => 'Propel\\Runtime\\Connection\\ConnectionWrapper',
                            'dsn' => 'pgsql:host=postgres;port=5432;dbname=docker',
                            'user' => 'docker',
                            'password' => 'docker',
                            'attributes' => [
                            ],
                        ],
                    ],
                ],
            ],
        ]
    ]
];
