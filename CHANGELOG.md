# Changelog

All notable changes to `supportik/supportik` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## 0.0.1 - TBA

*Setup working project.*

<!--

Template - see: https://keepachangelog.com/en/1.0.0/

## [0.0.2] - YYYY-MM-DD

### Added
- Nothing

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing

[0.0.2]: https://gitlab.com/supportik/supportik/compare?from=v0.0.1&to=v0.0.2

-->
