@echo off

@FOR /f "tokens=*" %%i IN ('docker-machine env') DO @%%i

SETLOCAL ENABLEDELAYEDEXPANSION
SET MATCH=c:\devel
SET REPLACE=/devel
SET INPUT_ARG=%*
SET "PHP_ARG=!INPUT_ARG:%MATCH%=%REPLACE%!"
SET "PHP_ARG=!PHP_ARG:\\=/!"
SET "PHP_ARG=!PHP_ARG:\=/!"

docker-compose exec supportik_serverside php %PHP_ARG%
