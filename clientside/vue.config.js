module.exports = {
  devServer: {
    hot: true,
    host: "localhost",
		proxy: "http://server:8080",
    disableHostCheck: true,
		watchOptions: {
			aggregateTimeout: 300,
			poll: 1000
		}
  }
}