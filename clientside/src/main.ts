import Vue from "vue";
import Notifications from "vue-notification";

import App from "./App.vue";
import router from "./router";
import store from "./store";

import { RequestPlugin } from "./request";

import "@/assets/css/tailwind.scss"

Vue.config.productionTip = false;

Vue.use(Notifications);
Vue.use(RequestPlugin);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
