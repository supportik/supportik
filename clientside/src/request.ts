import Vue, { VueConstructor } from "vue";
import router from "@/router";

const url = window.location.protocol + "//" + window.location.hostname + ":8080";

export function RequestPlugin(vue: VueConstructor): void {
  vue.prototype.$request = request.bind(window);
  vue.prototype.$request.get = request.bind(window, "GET");
  vue.prototype.$request.post = request.bind(window, "POST");
}

declare module 'vue/types/vue' {
    interface Vue {
        $request: Request;
    }
}

interface Request {
  (): any;
  get: Function;
  post: Function;
}

export async function request<Request>(method: String, path: String, data?: any, headers?: any): Promise<any> {
  path = path.replace(/^\//, "");

  try {
    let response = await fetch(url + "/api/" + path, {
      method: method.toUpperCase(),
      credentials: "include",
      cache: "no-cache",
      headers: Object.assign(
        {
          "content-type": "application/json"
        },
        headers
      ),
      body: JSON.stringify(data)
    });

    return await parseAPIResponse(response);
  } catch (error) {
    return handleError(error);
  }
}

async function parseAPIResponse(response: Response) {
  let text = await response.text();
//   let data = await response.json();

  if (!text) {
    throw new Error("Response is empty.");
  }

  let data = JSON.parse(text);

  if (response.status === 401) {
    // Unauthorized
    router.push({path: '/login', query: { redirectTo: router.currentRoute.path}});
    throw new Error(data.Error.Message);
  } else if (!response.ok) {
    console.error(response);
    throw new Error("Not OK!");
  }

  // TODO check returned data?

  if (data.Redirect) {
    console.error("Redirect!");
    router.push(data.Redirect);
    throw new Error("Redirect");
  }

  return data.Payload;
}

function handleError(error: Error) {
  alert(error.message);

  throw error;
}
