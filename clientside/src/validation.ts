import { Vue, Component } from "vue-property-decorator";

function doValidate(field: Field, validators: Validator[]) {
  for (var i = 0, validator, valid; i < validators.length; i++) {
    validator = validators[i];
    valid = validator.valid(field);

    if (validator.allowEmpty && Helpers.isEmpty(field.value)) {
      return null;
    } else if (!valid) {
      if (validator.description) {
        return [field.name, validator.definition, validator.description].join(" ");
      } else {
        return field.name+ " is invalid";
      }
    }
  }

  return null;
}

class Field {
  public name: string;
  public path: string;
  public validators: Validator[];

  public value: any = null;
  public other: Map<string, any> = new Map<string, any>();
  public message: string | null = null;
  public valid: boolean = true;
  public required: boolean = false;

  public constructor(name: string, path: string, validators: Validator[]) {
    this.name = name;
    this.path = path;
    this.validators = validators;
  }
}

abstract class Validator {
  // <field name> <definition> <description>
  public definition: string = "must be";
  public description: string | null = null;

  public allowEmpty: boolean = true;
  public other: string | null = null;

  public abstract valid(field: Field): boolean;
}

abstract class RegexValidator extends Validator {
  public abstract regex: RegExp;

  public valid(field: Field): boolean {
    return Helpers.toString(field.value).match(this.regex) !== null;
  }
}

class Helpers {
  public static isEmpty(obj: any): boolean {
    return (
      !obj ||
      (typeof obj === "string" && String.prototype.trim.call(obj).length === 0) ||
      (Array.isArray(obj) && obj.length === 0)
    );
  }
  public static isFn(obj: any): boolean {
    return !!(obj && obj.constructor && obj.call && obj.apply);
  }
  public static isValidator(obj: any): boolean {
    if (typeof obj !== "object" || !Helpers.isFn(obj.valid)) {
      console.warn("Invalid validator:", obj);
      return false;
    }

    return true;
  }
  public static toString(obj: any): string {
    if (typeof obj === "string") {
      return obj;
    } else if (typeof obj === "object") {
      if (obj.toString) {
        return obj.toString();
      } else {
        return Object.prototype.toString.call(obj);
      }
    } else if (typeof obj === "number") {
      return Number.prototype.toString.call(obj);
    } else {
      return "";
    }
  }
  public static humanReadableList(list: string[]): string {
    let last = list.pop();

    if (!last) {
      return '';
    } else if (!list.length) {
      return last;
    }

    list.push([list.pop(), last].join(" or "));

    return list.join(", ");
  }
}

interface Validators {
  required: Validator;
  requiredIf: (other: string) => Validator;
  requiredUnless: (other: string) => Validator;
  equal: (value: any) => Validator;
  regex: (regex: RegExp, description: string) => Validator;
  integer: Validator;
  between: (min: Number, max: Number) => Validator;
  email: Validator;
  minLength: (min: Number) => Validator;
  maxLength: (max: Number) => Validator;
  or: (...validators: Validator[]) => Validator;
  and: (...validators: Validator[]) => Validator;
}

export const Validators = new (class implements Validators {
  required = new (class extends Validator {
    definition: string = "is";
    description: string = "required";
    allowEmpty: boolean = false;

    public valid(field: Field): boolean {
      field.required = true;
      return !Helpers.isEmpty(field.value);
    };
  })();
  requiredIf = (other: string) =>
    new (class extends Validator {
      definition: string = "is";
      description: string = "required";
      allowEmpty: boolean = false;
      other: string = other;

      public valid(field: Field): boolean {
        field.required = !Helpers.isEmpty(field.other.get(this.other));

        return (
          !field.required ||
          !Helpers.isEmpty(field.value)
        );
      };
    })();
  requiredUnless = (other: string) =>
    new (class extends Validator {
      definition: string = "is";
      description: string = "required";
      allowEmpty: boolean = false;
      other: string = other;

      public valid(field: Field): boolean {
        field.required = Helpers.isEmpty(field.other.get(this.other));

        return (
          field.required ||
          !Helpers.isEmpty(field.value)
        );
      };
    })();
  equal = (value: any) =>
    new (class extends Validator {
      public valid(field: Field): boolean {
        return Helpers.toString(field.value) === Helpers.toString(value);
      };
    });
  regex = (regex: RegExp, description: string) =>
    new (class extends RegexValidator {
      public regex: RegExp = regex;
      public description: string = description;
    })();
  integer = new (class extends RegexValidator {
    public regex: RegExp = /^[+-]?[0-9]+$/;
    public descrption: string = "a whole number";
  })();
  between = (min: Number, max: Number) => new (class extends Validator {
    public description = "between " +min+ " and " +max;
    public valid (field: Field): boolean {
      return (field.value >= min) && (field.value <= max);
    }
  })();
  email = new (class extends RegexValidator {
    public regex: RegExp = /^[\w.\-_]*\w@[\w\-_]+(\.\w{2,})+$/i;
    public descrption: string = "a valid email address";
  })();

  minLength = (min: Number) => new (class extends Validator {
    public description = "at least " + min + " character" + (min == 1 ? "" : "s");
    public valid (field: Field): boolean {
      return Helpers.toString(field.value).length >= min;
    }
  })();
  maxLength = (max: Number) => new (class extends Validator {
    public description = "at most " + max + " character" + (max == 1 ? "" : "s");
    public valid (field: Field): boolean {
      return Helpers.toString(field.value).length <= max;
    }
  })();

  or = (...validators: Validator[]) =>
    new (class extends Validator {
      private validators: Validator[] = validators.filter(Helpers.isValidator);

      public constructor() {
        super();

        this.buildDescription();
      }

      public valid(field: Field): boolean {
        if (!this.validators.length) {
          return true;
        }

        for (var i = 0; i < this.validators.length; i++) {
          if (this.validators[i].valid(field)) {
            return true;
          }
        }

        return false;
      };

      private buildDescription(): string {
        var description: string[] = [];
        for (var i = 0; i < this.validators.length; i++) {
          if (this.validators[i].description) {
            description.push(this.validators[i].description || '');
          }
        }

        return Helpers.humanReadableList(description);
      }
    });
  and = (...validators: Validator[]) =>
    new (class extends Validator {
      private validators: Validator[] = validators.filter(Helpers.isValidator);

      public constructor() {
        super();

        this.buildDescription();
      }

      public valid(field: Field): boolean {
        if (!this.validators.length) {
          return true;
        }

        for (var i = 0; i < this.validators.length; i++) {
          if (this.validators[i].valid(field)) {
            return true;
          }
        }

        return false;
      };

      private buildDescription(): string {
        var description: string[] = [];
        for (var i = 0; i < this.validators.length; i++) {
          if (this.validators[i].description) {
            description.push(this.validators[i].description || '');
          }
        }

        return Helpers.humanReadableList(description);
      }
    });
});

// ValidateVue.Validators.integerID =
//   ValidateVue.Validators.regex(/^[0-9]+$/, "an ID");
// ValidateVue.Validators.numeric =
//   ValidateVue.Validators.regex(/^[+-]?[0-9,\.]+$/, "a number");
// ValidateVue.Validators.decimal =
//   ValidateVue.Validators.regex(/^[+-]?[0-9,]+\.[0-9]+$/, "a decimal");
// ValidateVue.Validators.maxLength = (x) => _create(
//   "at most " +x+ " character" +(x == 1 ? "" : "s"),
//   function (field) {
//     return (toString(field.value).length <= x);
//   }
// );
// ValidateVue.Validators.minValue = (x) => _create(
//   "greater than " +x,
//   function (field) {
//     return (field.value >= x);
//   }
// );
// ValidateVue.Validators.maxValue = (x) => _create(
//   "less than " +x,
//   function (field) {
//     return (field.value <= x);
//   }
// );
// ValidateVue.Validators.sameAs = (other) => _create(
//   "the same as " +other,
//   function (field) {
//     return (field.value === field.other[other]);
//   },
//   other
// );
// ValidateVue.Validators.alpha = _create(
//   "only letters",
//   function (field) {
//     return (
//       toString(field.value)
//         .normalize("NFD")
//         .match(/^[a-z\u0300-\u036F]*$/i)
//       !== null
//     );
//   }
// );
// ValidateVue.Validators.alphaNumeric = _create(
//   "only letters",
//   function (field) {
//     return (
//       toString(field.value)
//         .normalize("NFD")
//         .match(/^[a-z0-9\u0300-\u036F]*$/i)
//       !== null
//     );
//   }
// );
// ValidateVue.Validators.ipAddress = ValidateVue.Validators
//   .regex(/^(1?[1-9]{1,2}|2[0-5]{2})(\.1?[1-9]{1,2}|2[0-5]{2}){3}$/, "a valid IP address");
// ValidateVue.Validators.guid = ValidateVue.Validators
//   .regex(/^\{?([0-9a-f]){8}-([0-9a-f]){4}-([0-9a-f]){4}-([0-9a-f]){4}-([0-9a-f]){12}\}?$/i, "a GUID");

// ValidateVue.Validators.not = function (v) {
//   if (!isValidator(v)) {
//     return _create("", () => true);
//   }

//   return _create(
//     v.def,
//     (field) => !v.valid(field),
//     null,
//     "must not be"
//   )
// };
// ValidateVue.Validators.debug = function (v) {
//   var old = v.valid;
//   v.valid = function (field) {
//     debugger;
//     var result = old(field);
//     return result;
//   }
//   return v;
// };

@Component
export class Validation extends Vue {
  public validation: any = {};

  public created(): boolean {
    // debugger;

    this.validation.$valid = {};
    this.validation.$invalid = {};
    this.validation.$all = false;

    this.setupValidations([], this.validation);

    return true;
  }

  private setupValidations(path: string[], validation: any): void {
    Object.entries(validation).forEach(([key, validators]: [string, any]) => {
      // debugger;
      if (validators instanceof Array) {
        let pathArr = path.concat(key);
        validation[key] = this.setupField(key, pathArr.join("."), validators.filter(Helpers.isValidator));
      } else if (validators instanceof Object) {
        this.setupValidations(path.concat(key), validators);
      }
    });
  }

  private setupField (name: string, path: string, validators: Validator[]): Field {
    let field = new Field(name, path, validators);

    var others: Map<string, Validator[]> = new Map<string, Validator[]>();
    for (var i = 0; i < validators.length; i++) {
      var v = validators[i];
      if (v.other) {
        others.set(v.other, (others.get(v.other) || []).concat(v));
      }
    }

    Object.entries(others).forEach(([other, validators]: [string, Validator[]]) => {
      field.other.set(other, this.$data[other]);

      this.ensureValueDefinedElseSet(other);

      this.$watch(other, (val: any, old: any) => {
        field.other.set(other, val);
        field.message = doValidate(field, validators);
        field.valid = (field.message === null);

        this.updateFieldTotal(field);
      });
    });

    this.ensureValueDefinedElseSet(path);

    this.$watch(path, (val: any, old: any) => {
      field.value = val;
      field.message = doValidate(field, validators);
      field.valid = (field.message === null);

      this.updateFieldTotal(field);
    }, {
      deep: true,
      immediate: true,
    });

    this.updateFieldTotal(field);

    return field;
  }

  private ensureValueDefinedElseSet(path: string): void {
    let pathParts = path.split(".");
    let last = pathParts.pop();

    let current: any = pathParts.reduce((previous: any, current: string) => {
      if (previous[current] === undefined) {
        Vue.set(previous, current, {});
      }

      return previous[current];
    }, this);

    if (last !== undefined && current[last] === undefined) {
      Vue.set(current, last, null);
    }
  }

  private updateFieldTotal (field: Field): void {
    if (field.valid) {
      this.validation.$valid[field.path] = true;
      delete this.validation.$invalid[field.path];
    } else {
      this.validation.$invalid[field.path] = field.message;
      delete this.validation.$valid[field.path];
    }

    this.validation.$all = (Object.keys(this.validation.$invalid).length === 0);
  }
}
