import { Module } from "vuex";
import { RootState, LoadingData } from "./types";
import { request } from "@/request";

const namespaced: boolean = true;

interface UserState {
  self: LoadingData<Object>,
  list: LoadingData<Array<Object>>,
  profile: LoadingData<Object>,
}

const state: UserState = {
	self:  new LoadingData({
		Loaded: false,
		Expires: null,
		User: null,
	}),
  list: new LoadingData([]),
  profile: new LoadingData({})
}

export const user: Module<UserState, RootState> = {
  namespaced,
  state,
  getters: {},
  actions: {
    async validate_session(context: any): Promise<void> {
      return context.state.self.update(
				request("GET", "/user/validate")
					.then(data => {
						return {
							Loaded: true,
							Expires: data.Expires,
							User: data.User
						};
					})
      );
    },
    async fetch_profile(context: any, id: Number): Promise<void> {
      return context.state.profile.update(
        request("GET", "/user/" + id)
          .then(data => data.User)
      );
    },
    async save_profile(context: any, user: Object): Promise<void> {
      return context.state.profile.update(
        request("POST", "/user", user)
          .then(data => data.User)
      );
    },
    async fetch_all(context: any): Promise<void> {
      return context.state.list.update(
        request("GET", "/user/list")
          .then(data => data.Users)
      );
    },
  },
  mutations: {}
};
