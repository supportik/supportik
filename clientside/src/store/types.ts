export interface RootState {

}

export class LoadingData<T> {
  public loading: Boolean = false;
  public data: T;

  public constructor(data: T) {
    this.data = data;
  }

  public set(data: T): void {
    this.data = data;
  }

  public async update(promise: Promise<T>): Promise<void> {
    this.loading = true;
    try {
      this.data = await promise;
    } finally {
      this.loading = false;
    }
  }
}
