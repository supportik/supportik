import { Module } from "vuex";
import { RootState, LoadingData } from "./types";
import { dashboard } from "./admin/dashboard";

const namespaced: boolean = true;

interface AdminState {}
const state: AdminState = {}

export const admin: Module<AdminState, RootState> = {
  namespaced,
  modules: {
    dashboard
  },
  state,
};
