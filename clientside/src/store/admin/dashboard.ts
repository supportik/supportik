import { Module, ActionTree } from "vuex";
import { RootState, LoadingData } from "../types";
import { request } from "@/request";

const namespaced: boolean = true;

interface AdminDashboardState {
	widgets: LoadingData<Array<Object>>;
}

const state: AdminDashboardState = {
	widgets: new LoadingData([]),
}

const actions: ActionTree<AdminDashboardState, RootState> = {
	fetch_widgets({ state }) {
		state.widgets.update(
			request('GET', '/admin/dashboard')
				.then(data => data.Widgets)
		);
	}
};

export const dashboard: Module<AdminDashboardState, RootState> = {
	namespaced,
	state,
	getters: {},
	actions,
	mutations: {
		clear(state: AdminDashboardState) {
			state.widgets.set([]);
		}
	}
};
