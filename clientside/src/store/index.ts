import Vue from "vue";
import Vuex, { StoreOptions } from "vuex";
import { RootState } from "./types";
import { nav } from "./nav";
import { user } from "./user";
import { admin } from "./admin";

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  modules: {
    admin, nav, user
  },
  state: {
    loading: false,
	}
};

export default new Vuex.Store(store);
