import { Module, ActionTree } from "vuex";
import { RootState, LoadingData } from "./types";
import { request } from "@/request";

const namespaced: boolean = true;

interface NavState {
	items: LoadingData<Array<Object>>
}

const state: NavState = {
	items: new LoadingData([]),
}

export const nav: Module<NavState, RootState> = {
	namespaced,
	state,
	getters: {
		items(state: NavState) {
			return state.items;
		}
	},
	actions: {
		reload({ state }) {
			state.items.update(
				request('GET', '/menu')
				.then(data => data.Items)
			);
		},
		reloadAdmin({ state }) {
			state.items.update(
				request('GET', '/admin/menu')
				.then(data => data.Items)
			);
		}
	},
	mutations: {
		clear(state: NavState) {
			state.items.set([]);
		}
	}
};
