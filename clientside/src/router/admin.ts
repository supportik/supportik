import Login from "../views/admin/AdminLogin.vue";
import Dashboard from "../views/admin/AdminDashboard.vue";

export default [
  {path: "", redirect: 'dashboard'},
  {path: "login", component: Login},
  {path: "dashboard", component: Dashboard}
];
