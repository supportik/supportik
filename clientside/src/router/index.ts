import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";

import AdminRoutes from "./admin";

import Parent from "../views/Parent.vue";
import Root from "../views/Root.vue";
import Invalid from "../views/Invalid.vue";
import Login from "../views/Login.vue";
import Logout from "../views/Logout.vue";
import Dashboard from "../views/Dashboard.vue";
import Profile from "../views/Profile.vue";

Vue.use(VueRouter);

const routes = [

	{
		path: "/",
		component: Parent,
		meta: {},
		children: [
			{path: "", redirect: 'app'},
			{
				path: "login",
				component: Login,
				beforeEnter: (to, from, next) => {
					if (!store.state.user.self.data.User) {
						next()
						return;
					}

					// TODO check expired

					next('redirectTo' in to.query ? to.query.redirectTo : '/');
				}
			},
			{path: "logout", component: Logout},
		]
	},

	{
		path: "/app",
		component: Parent,
		meta: {
			requiresAuth: true,
		},
		children: [
			{path: "", redirect: 'dashboard'},
			{path: "dashboard", component: Dashboard},
			{path: "profile/:id", component: Profile, props: true},
		]
	},

	{
		path: "/admin",
		component: Parent,
		meta: {
			requiresAuth: true,
			isAdmin: true,
		},
		children: AdminRoutes
	},

	{path: "*", component: Invalid}
];

const router = new VueRouter({
	mode: "history",
	base: process.env.BASE_URL,
	routes
});

router.beforeEach(async (to, from, next) => {
	const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

	await store.dispatch('user/validate_session');

	if (requiresAuth && !store.state.user.self.data.User) {
		next("/login?redirectTo=" + encodeURIComponent(to.fullPath));
		return;
	}

	next();
});

export default router;
